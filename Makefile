CSS = \
	custom.css

FILTERS = \
	email-libravatar.lua

all: $(CSS)

V_GEN = $(_v_GEN_$(V))
_v_GEN_ = $(_v_GEN_0)
_v_GEN_0 = @echo "  GEN     " $@;

edit = $(V_GEN) m4 -P $@.in >$@ && chmod g-w $@

%: %.in
	$(edit)

%.sh: %.sh.in
	$(edit)

clean:
	$(RM) $(CSS)

deploy: all
	install -m644 $(CSS) /usr/share/cgit/
	install -m644 $(FILTERS) /usr/lib/cgit/filters/

.PHONY: all clean
